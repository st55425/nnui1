﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;

namespace Cviceni3
{
     public class PuzzleState
    {
        public static int PUZZLE_SIZE { get; } = 3;

        public int[,] State { get;private set; }
        private int horizontalEmptyCellIndex;
        private int verticalEmptyCellIndex;

        private PuzzleState(int[,] state)
        {
            State = state;
            for (int i = 0; i < PUZZLE_SIZE; i++)
            {
                for (int j = 0; j < PUZZLE_SIZE; j++)
                {
                    if (State[i,j] == 0)
                    {
                        verticalEmptyCellIndex = i;
                        horizontalEmptyCellIndex = j;
                    }
                }
            }
        }

        public PuzzleState(PuzzleState original) 
        {
            State = original.State.Clone() as int[,];
            horizontalEmptyCellIndex = original.horizontalEmptyCellIndex;
            verticalEmptyCellIndex = original.verticalEmptyCellIndex;

        }

        public static PuzzleState CreatePuzzleState(int[,] state)
        {
            if (ValidateState(state))
            {
                return new PuzzleState(state);
            }
            throw new ArgumentException("Stav hlavolamu není validní");
        }

        public static bool ValidateState(int[,] state)
        {
            if (state.Length != PUZZLE_SIZE*PUZZLE_SIZE || state.Rank != PUZZLE_SIZE-1)
            {
                return false;
            }
            if(state.Cast<int>().Any(x => x < 0 && x > PUZZLE_SIZE*PUZZLE_SIZE-1))
            {
                return false;
            }

            HashSet<int> nonDuplicate = new HashSet<int>();
            foreach (var number in state)
            {
                if (!nonDuplicate.Add(number))
                {
                    return false;
                }  
            }
            return true;
        }

        private PuzzleState MoveUp()
        {
            if (!this.CanMoveUp())
            {
                throw new ArgumentException("Posun nahoru není možný");
            }
            PuzzleState newState = new PuzzleState(this);
            newState.State[newState.verticalEmptyCellIndex, newState.horizontalEmptyCellIndex] =
                newState.State[newState.verticalEmptyCellIndex - 1, newState.horizontalEmptyCellIndex];
            newState.State[newState.verticalEmptyCellIndex - 1, newState.horizontalEmptyCellIndex] = 0;
            newState.verticalEmptyCellIndex--;
            return newState;
        }

        private PuzzleState MoveDown()
        {
            if (!this.CanMoveDown())
            {
                throw new ArgumentException("Posun dolů není možný");
            }
            PuzzleState newState = new PuzzleState(this);
            newState.State[newState.verticalEmptyCellIndex, newState.horizontalEmptyCellIndex] =
                newState.State[newState.verticalEmptyCellIndex + 1, newState.horizontalEmptyCellIndex];
            newState.State[newState.verticalEmptyCellIndex + 1, newState.horizontalEmptyCellIndex] = 0;
            newState.verticalEmptyCellIndex++;
            return newState;
        }

        private PuzzleState MoveLeft()
        {
            if (!this.CanMoveLeft())
            {
                throw new ArgumentException("Posun doleva není možný");
            }
            PuzzleState newState = new PuzzleState(this);
            newState.State[newState.verticalEmptyCellIndex, newState.horizontalEmptyCellIndex] =
                newState.State[newState.verticalEmptyCellIndex, newState.horizontalEmptyCellIndex -1];
            newState.State[newState.verticalEmptyCellIndex, newState.horizontalEmptyCellIndex -1] = 0;
            newState.horizontalEmptyCellIndex--;
            return newState;
        }

        private PuzzleState MoveRight()
        {
            if (!this.CanMoveRight())
            {
                throw new ArgumentException("Posun doprava není možný");
            }
            PuzzleState newState = new PuzzleState(this);
            newState.State[newState.verticalEmptyCellIndex, newState.horizontalEmptyCellIndex] =
                newState.State[newState.verticalEmptyCellIndex, newState.horizontalEmptyCellIndex + 1];
            newState.State[newState.verticalEmptyCellIndex, newState.horizontalEmptyCellIndex + 1] = 0;
            newState.horizontalEmptyCellIndex++;
            return newState;
        }

        private bool CanMoveUp()
        {
            return verticalEmptyCellIndex != 0;
        }

        private bool CanMoveDown()
        {
            return verticalEmptyCellIndex != PUZZLE_SIZE-1;
        }

        private bool CanMoveLeft()
        {
            return horizontalEmptyCellIndex != 0;
        }

        private bool CanMoveRight()
        {
            return horizontalEmptyCellIndex != PUZZLE_SIZE - 1;
        }

        public bool CanMove(TransitionAction action)
        {
            return action switch
            {
                TransitionAction.Up => CanMoveUp(),
                TransitionAction.Down => CanMoveDown(),
                TransitionAction.Left => CanMoveLeft(),
                TransitionAction.Right => CanMoveRight(),
                _ => false,
            };
        }

        public PuzzleState Move(TransitionAction action)
        {
            return action switch
            {
                TransitionAction.Up => MoveUp(),
                TransitionAction.Down => MoveDown(),
                TransitionAction.Left => MoveLeft(),
                TransitionAction.Right => MoveRight(),
                _ => throw new ArgumentException("Nedefinovaná akce"),
            };
        }

        public override bool Equals(object obj)
        {
            if (obj is PuzzleState compare)
            {
                return State.Rank == compare.State.Rank &&
                    Enumerable.Range(0, State.Rank).All(dimension => State.GetLength(dimension) == compare.State.GetLength(dimension)) &&
                    State.Cast<int>().SequenceEqual(compare.State.Cast<int>());
        
            }
            return false;       
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(ToString());
        }

        public override string ToString()
        {
            StringBuilder builder = new StringBuilder();
            for (int i = 0; i < PUZZLE_SIZE; i++)
            {
                for (int j = 0; j < PUZZLE_SIZE; j++)
                {
                    builder.Append(State[i, j]);
                    if (j < PUZZLE_SIZE - 1)
                    {
                        builder.Append(" ");
                    }
                }
                if (i < PUZZLE_SIZE-1)
                {
                    builder.Append("\n");
                }
            }
            return builder.ToString();
        }
    }
}
