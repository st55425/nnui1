﻿
using System;
using System.Collections.Generic;
using System.Text;

namespace Cviceni3
{
    class GraphNode
    {

        private static int IdCounter = 1;

        public int Id { get;}
        public PuzzleState State { get;private set; }
        public GraphNode Parent { get;private set; }
        public TransitionAction Action { get;private set; }
        public int Depth { get;private set; }

        public GraphNode(PuzzleState state, GraphNode parent, TransitionAction action)
        {
            Id = IdCounter++;
            State = state;
            Parent = parent;
            Action = action;
            if (parent == null)
            {
                Depth = 0;
            }
            else
            {
                Depth = parent.Depth + 1;
            }
        }

        public override string ToString()
        {
            return "Move " + Action + "\n\n" + State.ToString() + "\n";
        }

        public override bool Equals(object obj)
        {
            return obj is GraphNode node &&
                   EqualityComparer<PuzzleState>.Default.Equals(State, node.State);                
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(State);
        }
    }
}
