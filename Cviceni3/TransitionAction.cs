﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Cviceni3
{
    public enum TransitionAction
    {
        None,
        Up,
        Down,
        Left,
        Right
    }
}
