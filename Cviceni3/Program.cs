﻿using System;

namespace Cviceni3
{
    class Program
    {
        static void WriteSolution(IterativeDepthSearcher searcher)
        {
            Console.WriteLine($"Number of expanded states: {searcher.ExpandedCount}");
            Console.WriteLine($"Maximum states in memory: {searcher.InMemoryMax}");
            if (searcher.SolutionExist)
            {
                Console.WriteLine($"Number of neccessary moves: {searcher.SolutionPath.Count}");
                foreach (var item in searcher.SolutionPath.Values)
                {
                    Console.WriteLine(item.ToString());
                }
            }
            else
            {
                Console.WriteLine("Solution does not exist");
            }
        }

        static void Main(string[] args)
        {
            PuzzleState startState1 = PuzzleState.CreatePuzzleState(new int[,] { { 7, 2, 4 }, { 5, 0, 6 }, { 8, 3, 1 } });
            PuzzleState startState2 = PuzzleState.CreatePuzzleState(new int[,] { { 8, 0, 6 }, { 5, 4, 7 }, { 2, 3, 1 } });
            PuzzleState startState3 = PuzzleState.CreatePuzzleState(new int[,] { { 1, 2, 3 }, { 8, 0, 4 }, { 7, 6, 5 } });
            PuzzleState tagretState = PuzzleState.CreatePuzzleState(new int[,] { { 5, 4, 1 }, { 2, 6, 7 }, { 0, 8, 3 } });
            IterativeDepthSearcher searcher1 = new IterativeDepthSearcher(startState1, tagretState);
            Console.WriteLine("Solution for first start state");
            WriteSolution(searcher1);
            IterativeDepthSearcher searcher2 = new IterativeDepthSearcher(startState2, tagretState);
            Console.WriteLine("Solution for second start state");
            WriteSolution(searcher2);
            IterativeDepthSearcher searcher3 = new IterativeDepthSearcher(startState3, tagretState);
            Console.WriteLine("Solution for third start state");
            WriteSolution(searcher3);
        }
    }
}
