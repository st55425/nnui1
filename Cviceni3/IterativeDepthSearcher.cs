﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cviceni3
{
    class IterativeDepthSearcher
    {
        private Stack<GraphNode> fringe;
        public bool SolutionExist { get; private set; }
        public Dictionary<GraphNode, TransitionAction> SolutionPath { get; private set; }
        public int ExpandedCount { get; private set; }
        public int InMemoryMax { get; private set; }
        private int MaxExpandedState;

        public IterativeDepthSearcher(PuzzleState origin, PuzzleState target)
        {
            SolutionExist = false;
            SolutionPath = new Dictionary<GraphNode, TransitionAction>();
            MaxExpandedState = SetMaxExpandedState();
            FindSolution(origin, target);
        }

        // omez maximum pomocí faktoriálu
        private void FindSolution(PuzzleState origin, PuzzleState target)
        {
            int depth = 1;
            while (!SolutionExist)
            {
                int thisIterationExpanded = FindSolutionInDepth(origin, target, depth);
                ExpandedCount += thisIterationExpanded;
                if (MaxExpandedState < thisIterationExpanded)
                {
                    return;
                }
                depth++;
            }
        }

        private int FindSolutionInDepth(PuzzleState origin, PuzzleState target, int depth)
        {
            int expanded = 0;
            fringe = new Stack<GraphNode>();
            SolutionPath = new Dictionary<GraphNode, TransitionAction>();
            fringe.Push(new GraphNode(origin, null, TransitionAction.None));
            int lastDepth = 0;
            while(fringe.Count > 0)
            {
                GraphNode current = fringe.Pop();

                if (SolutionPath.ContainsKey(current))
                {
                    continue;
                }
                if (current.Depth <= lastDepth)
                {
                    RemoveNodesFromPath(current.Depth);
                }
                if (current.State.Equals(target))
                {
                    SolutionExist = true;
                    return expanded;
                }
                if (current.Depth < depth)
                {
                    expanded++;
                    FindAllSuccessors(current);
                    CheckInMemoryMax();
                    SolutionPath.Add(current, current.Action);
                }
                lastDepth = current.Depth;
            }
            return expanded;
        }

        private void FindAllSuccessors(GraphNode origin)
        {
            foreach (TransitionAction action in Enum.GetValues(typeof(TransitionAction)))
            {
                if (origin.State.CanMove(action))
                {
                    GraphNode successor = new GraphNode(origin.State.Move(action), origin, action);
                    if (!fringe.Contains(successor))
                    {
                        fringe.Push(successor);
                    }
                }
            }
        }

        private void RemoveNodesFromPath(int maxDepth)
        {
            foreach (var item in SolutionPath.Keys)
            {
                if (item.Depth >= maxDepth)
                {
                    SolutionPath.Remove(item);
                }
            }
        }

        private void CheckInMemoryMax()
        {
            int inMemory = fringe.Count + SolutionPath.Count;
            if (inMemory > InMemoryMax)
            {
                InMemoryMax = inMemory;
            }
        }

        private int SetMaxExpandedState()
        {
            int max = 1;
            for (int i = 1; i <= Math.Pow(PuzzleState.PUZZLE_SIZE, 2); i++)
            {
                max *= i;
            }
            return max;
        }
    }
}
