﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Cviceni1
{
    class TransitionSystem
    {
        public Dictionary<PuzzleState, Dictionary<TransitionAction, PuzzleState>> AllStates { get; private set; }
        public int StatesCount { get => AllStates.Count; }
        public int TransitionsCount { get;private set; }

        private Stack<PuzzleState> unprocessedStates;

        public TransitionSystem(PuzzleState initialState)
        {
            AllStates = new Dictionary<PuzzleState, Dictionary<TransitionAction, PuzzleState>>();
            unprocessedStates = new Stack<PuzzleState>();
            createCompleteSystem(initialState);
        }

        private void createCompleteSystem(PuzzleState initialState)
        {
            unprocessedStates.Push(initialState);
            while(unprocessedStates.Count > 0)
            {
                PuzzleState currentState = unprocessedStates.Pop();
                if (!AllStates.ContainsKey(currentState))
                {
                    AllStates.Add(currentState, createAllSuccessors(currentState));
                }           
            } 
        }

        private Dictionary<TransitionAction, PuzzleState> createAllSuccessors(PuzzleState state)
        {
            Dictionary<TransitionAction, PuzzleState> successors = new Dictionary<TransitionAction, PuzzleState>();
            foreach (TransitionAction action in TransitionAction.GetValues(typeof(TransitionAction)))
            {
                if (state.CanMove(action))
                {
                    PuzzleState newState = state.Move(action);
                    successors.Add(action, newState);
                    TransitionsCount++;
                    if (!AllStates.ContainsKey(newState))
                    {
                        unprocessedStates.Push(newState);
                        
                    }               
                }
            }
            return successors;
        }

    }
}

