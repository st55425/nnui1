﻿using System;

namespace Cviceni1
{
    class Program
    {
        static void Main(string[] args)
        {
            PuzzleState startState = PuzzleState.CreatePuzzleState(new int[,] { { 7, 2, 4 }, { 5, 0, 6 }, { 8, 3, 1 } });
            TransitionSystem system = new TransitionSystem(startState);
            Console.WriteLine($"Celkový počet stavů: {system.StatesCount}");
            Console.WriteLine($"Celkový počet možných přechodů mezi stavy: {system.TransitionsCount}");
        }
    }
}
