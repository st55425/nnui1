﻿using System;

namespace Cviceni2
{
    class Program
    {
        static void WriteSolution(BreadthFirstSearcher searcher)
        {
            Console.WriteLine($"Number of expanded states: {searcher.ExpandedCount}");
            if (searcher.SolutionExist)
            {
                Console.WriteLine($"Number of neccessary moves: {searcher.PathLength}");
                foreach (var item in searcher.SolutionPath)
                {
                    Console.WriteLine(item.ToString());
                }
            }
            else
            {
                Console.WriteLine("Solution does not exist");
            }
        }

        static void Main(string[] args)
        {
            PuzzleState startState1 = PuzzleState.CreatePuzzleState(new int[,] { { 7, 2, 4 }, { 5, 0, 6 }, { 8, 3, 1 } });
            PuzzleState startState2 = PuzzleState.CreatePuzzleState(new int[,] { { 8, 0, 6 }, { 5, 4, 7 }, { 2, 3, 1 } });
            PuzzleState startState3 = PuzzleState.CreatePuzzleState(new int[,] { { 1, 2, 3 }, { 8, 0, 4 }, { 7, 6, 5 } });
            PuzzleState tagretState = PuzzleState.CreatePuzzleState(new int[,] { { 5, 4, 1 }, { 2, 6, 7 }, { 0, 8, 3 } });
            PuzzleState tagretState2 = PuzzleState.CreatePuzzleState(new int[,] { { 0, 1, 2 }, { 3, 4, 5 }, { 6, 7, 8 } });
            BreadthFirstSearcher searcher1 = new BreadthFirstSearcher(startState1, tagretState);
            BreadthFirstSearcher searcher2 = new BreadthFirstSearcher(startState2, tagretState);
            BreadthFirstSearcher searcher3 = new BreadthFirstSearcher(startState3, tagretState);
            BreadthFirstSearcher searcher4 = new BreadthFirstSearcher(startState1, tagretState2);
            Console.WriteLine("Solution for first start state");
            WriteSolution(searcher1);
            Console.WriteLine("Solution for second start state");
            WriteSolution(searcher2);
            Console.WriteLine("Solution for third start state");
            WriteSolution(searcher3);
            Console.WriteLine("Solution for first start state, second target state");
            WriteSolution(searcher4);
        }
    }
}
