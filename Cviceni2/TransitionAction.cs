﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Cviceni2
{
    public enum TransitionAction
    {
        None,
        Up,
        Down,
        Left,
        Right
    }
}
