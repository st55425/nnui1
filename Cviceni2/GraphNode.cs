﻿
using System;
using System.Collections.Generic;
using System.Text;

namespace Cviceni2
{
    class GraphNode
    {

        private static int IdCounter = 1;

        public int Id { get;}
        public PuzzleState State { get;private set; }
        public GraphNode Parent { get;private set; }
        public TransitionAction Action { get;private set; }

        public GraphNode(PuzzleState state, GraphNode parent, TransitionAction action)
        {
            Id = IdCounter++;
            State = state;
            Parent = parent;
            Action = action;
        }

        public override string ToString()
        {
            return "Move " + Action + "\n\n" + State.ToString() + "\n";
        }
    }
}
