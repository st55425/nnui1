﻿
using System;
using System.Collections.Generic;
using System.Text;

namespace Cviceni2
{
    class BreadthFirstSearcher
    {
        private Queue<GraphNode> fringe;
        private HashSet<PuzzleState> explored;
        public bool SolutionExist { get => SolutionPath.Count >0; }
        public List<GraphNode> SolutionPath { get;private set; }
        public int ExpandedCount { get;private set; }
        public int PathLength { get => SolutionPath.Count-1; }

        public BreadthFirstSearcher(PuzzleState origin, PuzzleState target)
        {
            fringe = new Queue<GraphNode>();
            explored = new HashSet<PuzzleState>();
            SolutionPath = FindSolution(origin, target);
        }

        private List<GraphNode> FindSolution(PuzzleState origin, PuzzleState target)
        {
            GraphNode currentNode = new GraphNode(origin, null, TransitionAction.None);
            fringe.Enqueue(currentNode);
            while(fringe.Count > 0)
            {
                currentNode = fringe.Dequeue();
                if (!explored.Contains(currentNode.State))
                {
                    ExpandedCount++;
                    explored.Add(currentNode.State);
                    if (checkNodes(findAllSuccessors(currentNode), target, out GraphNode finalNode))
                    {
                        return reconstructPath(finalNode);
                    }                
                }          
            }

            return new List<GraphNode>();
        }

        private List<GraphNode> findAllSuccessors(GraphNode origin)
        {
            List<GraphNode> successors = new List<GraphNode>();
            foreach (TransitionAction action in Enum.GetValues(typeof(TransitionAction)))
            {
                if (origin.State.CanMove(action))
                {
                    successors.Add(new GraphNode(origin.State.Move(action), origin, action));
                }
            }
            return successors;
        }

        private bool checkNodes(List<GraphNode> nodes, PuzzleState target, out GraphNode finalNode)
        {
            foreach (var node in nodes)
            {
                if (node.State.Equals(target))
                {
                    finalNode = node;
                    return true;
                }
                if (!explored.Contains(node.State))
                {
                    fringe.Enqueue(node);
                }
            }

            finalNode = null;
            return false;
        }

        private List<GraphNode> reconstructPath(GraphNode finalNode)
        {
            List<GraphNode> path = new List<GraphNode>();
            GraphNode tempNode = finalNode;
            while(tempNode != null)
            {
                path.Add(tempNode);
                tempNode = tempNode.Parent;
            }
            path.Reverse();
            return path;
        }
    }
}
