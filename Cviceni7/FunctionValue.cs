﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Cviceni7
{
    public class FunctionValue
    {
        public double X { get;private set; }

        public double Y { get; private set; }

        public double Value { get; private set; }

        public FunctionValue(double x, double y, double value)
        {
            X = x;
            Y = y;
            Value = value;
        }
    }
}
