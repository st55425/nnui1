﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Cviceni7
{
    public class HillClimbingMinimumFinder : MinimumFinder
    {
        public int MaxIteration { get;private set; }
        public double SurroundingsSize { get;private set; }
        public int PointCount { get;private set; }

        public HillClimbingMinimumFinder(AbstractFunction function, int maxIterations, double surroundings) : base(function)
        {
            MaxIteration = maxIterations;
            SurroundingsSize = surroundings;
            PointCount = 50;
        }

        public override FunctionValue FindMinimum()
        {
            FunctionValue bestSolution = EvaluateRandomFunctionPoint(Function.XMin, Function.XMax, Function.YMin, Function.YMax);
            for (int i = 0; i < MaxIteration; i++)
            {
                for (int j = 0; j < PointCount; j++)
                {
                    double x = bestSolution.X;
                    double y = bestSolution.Y;
                    FunctionValue functionValue = EvaluateRandomFunctionPoint(
                        Math.Max(x - SurroundingsSize, Function.XMin), Math.Min(x + SurroundingsSize, Function.XMax),
                        Math.Max(y - SurroundingsSize, Function.YMin), Math.Min(y + SurroundingsSize, Function.YMax));
                    if (bestSolution.Value > functionValue.Value)
                    {
                        bestSolution = functionValue;
                    }
                }
            }
            return bestSolution;
        }

    }
}
