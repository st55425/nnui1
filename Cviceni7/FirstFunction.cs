﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Cviceni7
{
    public class FirstFunction : AbstractFunction
    {
        public FirstFunction(double xMin, double xMax, double yMin, double yMax) : base(xMin, xMax, yMin, yMax)
        {
        }

        public override double Evaluate(double x, double y)
        {
            if (x > XMax || x < XMin || y >YMax || y <YMin)
            {
                throw new ArgumentOutOfRangeException("Hodnota funkce je mimo hranice");
            }
            double value = Math.Pow(1.5 - x + x * y, 2);
            value += Math.Pow(2.25 - x + x * Math.Pow(y,2), 2);
            value += Math.Pow(2.625 - x + x * Math.Pow(y, 3), 2);
            return value;
        }
    }
}
