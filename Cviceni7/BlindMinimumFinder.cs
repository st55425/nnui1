﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Cviceni7
{
    public class BlindMinimumFinder : MinimumFinder
    {
        public int MaxIteration { get;private set; }
        public BlindMinimumFinder(AbstractFunction function, int maxIteration) : base(function)
        {
            MaxIteration = maxIteration;
            random = new Random();
        }

        public override FunctionValue FindMinimum()
        {
            FunctionValue bestSolution = new FunctionValue(0,0,double.MaxValue);
            for (int i = 0; i < MaxIteration; i++)
            {
                FunctionValue functionValue = EvaluateRandomFunctionPoint(Function.XMin, Function.XMax, Function.YMin, Function.YMax);
                if (functionValue.Value < bestSolution.Value)
                {
                    bestSolution = functionValue;
                }
            }
            return bestSolution;
        }
    }
}
