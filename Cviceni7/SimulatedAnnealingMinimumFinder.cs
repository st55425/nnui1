﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Cviceni7
{
    public class SimulatedAnnealingMinimumFinder : MinimumFinder
    {
        public double TemperatureMax { get;private set; }
        public double TemperatureMin { get; private set; }
        public double TemperatureCoefficient { get;private set; }
        public double SurroundingsSize { get;private set; }

        public SimulatedAnnealingMinimumFinder(AbstractFunction function, double temperatureMax, double temperatureMin, double temperatureCoefficient, double surroundingsSize) : base(function)
        {
            TemperatureMax = temperatureMax;
            TemperatureMin = temperatureMin;
            TemperatureCoefficient = temperatureCoefficient;
            SurroundingsSize = surroundingsSize;
        }

        public override FunctionValue FindMinimum()
        {
            FunctionValue bestSolution = EvaluateRandomFunctionPoint(Function.XMin, Function.XMax, Function.YMin, Function.YMax);
            double currentTemperature = TemperatureMax;
            while (currentTemperature > TemperatureMin)
            {
                FunctionValue functionValue = EvaluateRandomFunctionPoint(
                        Math.Max(bestSolution.X - SurroundingsSize, Function.XMin), Math.Min(bestSolution.X + SurroundingsSize, Function.XMax),
                        Math.Max(bestSolution.Y - SurroundingsSize, Function.YMin), Math.Min(bestSolution.Y + SurroundingsSize, Function.YMax));
                if (bestSolution.Value > functionValue.Value)
                {
                    bestSolution = functionValue;
                }
                else if (random.NextDouble() < Math.Pow(Math.E, (-functionValue.Value - bestSolution.Value)/currentTemperature))
                {
                    bestSolution = functionValue;
                }
                currentTemperature *= TemperatureCoefficient;
            }
            return bestSolution;
        }
    }
}
