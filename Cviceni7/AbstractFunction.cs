﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Cviceni7
{
    public abstract class AbstractFunction
    {
        public double XMin { get;private set; }

        public double XMax { get;private set; }

        public double YMin { get;private set; }

        public double YMax { get;private set; }

        protected AbstractFunction(double xMin, double xMax, double yMin, double yMax)
        {
            XMin = xMin;
            XMax = xMax;
            YMin = yMin;
            YMax = yMax;
        }

        public abstract double Evaluate(double x, double y);

    }
}
