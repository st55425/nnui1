﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Cviceni7
{
    public abstract class MinimumFinder
    {
        protected static Random random = new Random();
        public AbstractFunction Function { get;private set; }

        protected MinimumFinder(AbstractFunction function)
        {
            Function = function;
        }

        public abstract FunctionValue FindMinimum();

        protected FunctionValue EvaluateRandomFunctionPoint(double xMin, double xMax, double yMin, double yMax)
        {
            double randomX = random.NextDouble() * (xMax - xMin) + xMin;
            double randomY = random.NextDouble() * (yMax - yMin) + yMin;
            return new FunctionValue(randomX, randomY, Function.Evaluate(randomX, randomY));
        }
    }
}
