﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Cviceni7
{
    class SecondFunction : AbstractFunction
    {

        public SecondFunction(double xMin, double xMax, double yMin, double yMax) : base(xMin, xMax, yMin, yMax)
        {
        }

        public override double Evaluate(double x, double y)
        {
            if (x > XMax || x < XMin || y > YMax || y < YMin)
            {
                throw new ArgumentOutOfRangeException("Hodnota funkce je mimo hranice");
            }

            double value = Math.Pow(Math.Pow(x, 2) + y + 11, 2);
            value += Math.Pow(x + Math.Pow(y, 2) - 7, 2);
            return value;
        }
    }
}
