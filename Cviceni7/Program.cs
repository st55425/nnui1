﻿using System;

namespace Cviceni7
{
    class Program
    {
        static void Main(string[] args)
        {
            FirstFunction firstFunction = new FirstFunction(-4.5, 4.5, -4.5, 4.5);
            SecondFunction secondFunction = new SecondFunction(-5, 5, -5, 5);
            Console.WriteLine("Evaluated by BlindMinimumFinder:");
            FindMinimum(new BlindMinimumFinder(firstFunction, 1000));
            FindMinimum(new BlindMinimumFinder(secondFunction, 1000));
            Console.WriteLine();

            Console.WriteLine("Evaluated by HillClimbingMinimumFinder:");
            FindMinimum(new HillClimbingMinimumFinder(firstFunction, 100, 0.1));
            FindMinimum(new HillClimbingMinimumFinder(secondFunction, 100, 0.1));
            Console.WriteLine();

            Console.WriteLine("Evaluated by SimulatedAnnealingMinimumFinder:");
            FindMinimum(new SimulatedAnnealingMinimumFinder(firstFunction, 1000, 20, 0.999, 0.3));
            FindMinimum(new SimulatedAnnealingMinimumFinder(secondFunction, 1000, 20, 0.999, 0.3));
        }

        static void FindMinimum(MinimumFinder finder)
        {
            FunctionValue minimum = finder.FindMinimum();
            Console.WriteLine($"minimum: [{minimum.X}, {minimum.Y}] value: {minimum.Value}");
        }
    }
}
