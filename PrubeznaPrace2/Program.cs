﻿using System;


namespace PrubeznaPrace2
{
    class Program
    {
        private const int DefaultPossibilitesCount = 14;
        private static GeneticAlgorithm algorithm;

        static void Main(string[] args)
        {
            TownReader.ReadTowns();
            algorithm = new GeneticAlgorithm();

            Console.WriteLine("Vítejte v aplikaci pro hledání nejkratší cesty problému obchodního cestujícího");
            while (true)
            {
                Console.WriteLine();
                WriteDefaultPossibilities();
                if (int.TryParse(Console.ReadLine(), out int choice) && choice > 0 && choice <= DefaultPossibilitesCount)
                {
                    switch (choice)
                    {
                        case 1:
                            WriteTowns();
                            break;
                        case 2:
                            WriteParameters();
                            break;
                        case 3:
                            FindShortestPath();
                            break;
                        case 4:
                            if (CheckInputParameter("Nastavení velikosti populace ", 20, 50000, out int populationSize))
                            {
                                algorithm.PopulationSize = populationSize;
                            }
                            break;
                        case 5:
                            algorithm.PopulationSize = GeneticAlgorithm.DefaultPopulationSize;
                            Console.WriteLine("Velikost populace nastavena na výchozí hodnotu");
                            break;
                        case 6:
                            if (CheckInputParameter("Nastavení procenta mutujících jedinců ", 0, 100, out double mutationRate))
                            {
                                algorithm.MutationRate = mutationRate/100;
                            }
                            break;
                        case 7:
                            algorithm.MutationRate = GeneticAlgorithm.DefaultMutationRate;
                            Console.WriteLine("Procento mutujících jedinců nastaveno na výchozí hodnotu");
                            break;
                        case 8:
                            if (CheckInputParameter("Nastavení počtu kol turnaje při výběru rodičů ", 1, 5, out int tournamentRounds))
                            {
                                algorithm.TournamentRounds = tournamentRounds;
                            }
                            break;
                        case 9:
                            algorithm.TournamentRounds = GeneticAlgorithm.DefaultTournamentRounds;
                            Console.WriteLine("Počet kol turnaje při výběru rodičů nastaven na výchozí hodnotu");
                            break;
                        case 10:
                            if (CheckInputParameter("Nastavení počtu obnov populace ", 0, 30, out int renewLimit))
                            {
                                algorithm.RenewLimit = renewLimit;
                            }
                            break;
                        case 11:
                            algorithm.RenewLimit = GeneticAlgorithm.DefaultRenewLimit;
                            Console.WriteLine("Počet obnov populace nastaven na výchozí hodnotu");
                            break;
                        case 12:
                            if (CheckInputParameter("Nastavení počtu generací určujících konvergenci ", 10, 100, out int generationRemembered))
                            {
                                algorithm.GenerationRemembered = generationRemembered;
                            }
                            break;
                        case 13:
                            algorithm.GenerationRemembered = GeneticAlgorithm.DefaultGenerationRemembered;
                            Console.WriteLine("počet generací určujících konvergenci nastaven na výchozí hodnotu");
                            break;
                        case 14:
                            return;
                        default:
                            Console.WriteLine("Neplatná volba");
                            break;
                    }
                }
                else
                {
                    Console.WriteLine("Neplatná volba");
                }
            }
        }

        private static void WriteDefaultPossibilities()
        {
            Console.WriteLine("Co chcete udělat?");
            Console.WriteLine("1. Vypsat všechna města");
            Console.WriteLine("2. Vypsat parametry algoritmu");
            Console.WriteLine("3. Vyhledat cestu");
            Console.WriteLine("4. Zadat velikost populace");
            Console.WriteLine("5. Změnit velikost populace na výchozí hodnotu");
            Console.WriteLine("6. Zadat procento mutace");
            Console.WriteLine("7. Změnit procento mutace na výchozí hodnotu");
            Console.WriteLine("8. Zadat počet kol turnaje při výběru rodičů");
            Console.WriteLine("9. Změnit počet kol turnaje při výběru rodičů na výchozí hodnotu");
            Console.WriteLine("10. Zadat počet obnov populace");
            Console.WriteLine("11. Změnit počet obnov populace na výchozí hodnotu");
            Console.WriteLine("12. Zadat počet generací určujících konvergenci");
            Console.WriteLine("13. Změnit počet generací určujících konvergenci na výchozí hodnotu");
            Console.WriteLine("14. Ukončit aplikaci");
        }

        private static void WriteTowns()
        {
            string towns = "";
            foreach (var town in Town.AllTowns)
            {
                towns += town.Name;
                towns += ", ";
            }
            Console.WriteLine(towns.Remove(towns.Length - 2));
        }

        private static void WriteParameters()
        {
            Console.WriteLine("Nastavení algoritmu:");
            Console.WriteLine($"Velikost populace: {algorithm.PopulationSize}");
            Console.WriteLine($"Procento mutujících jedinců: {algorithm.MutationRate*100} %");
            Console.WriteLine($"Počet kol turnaje při výběru rodičů: {algorithm.TournamentRounds}");
            Console.WriteLine($"Počet obnov populace před ukončením algoritmu: {algorithm.RenewLimit}");
            Console.WriteLine($"Počet generací určujících konvergenci: {algorithm.GenerationRemembered}");
        }

        private static void FindShortestPath()
        {
            Console.WriteLine("Vyhledávám nejkratší cestu");
            algorithm.Evolution();
            Console.WriteLine($"Nejkratší nalezená cesta: {algorithm.BestSolution}");
            Console.WriteLine($"Celková uražená vzdálenost: {algorithm.BestSolution.Distance}");
            Console.WriteLine($"Počet generací využitých pro nalezení cesty: {algorithm.Generation}");
        }

        private static bool CheckInputParameter(string message, int from, int to, out int number)
        {
            while (true)
            {
                Console.WriteLine(message);
                Console.WriteLine($"Zadejte kladné celé číslo od {from} do {to}");
                if (int.TryParse(Console.ReadLine(), out number) && number >= from && number < to)
                {
                    return true;
                }
                else if (Confirmation("Zadáno nevalidní číslo, zkusit znovu?"))
                {
                    continue;
                }
                else {
                    return false;
                }
            }            
        }

        private static bool CheckInputParameter(string message, int from, int to, out double number)
        {
            while (true)
            {
                Console.WriteLine(message);
                Console.WriteLine($"Zadejte kladné (desetinné) číslo od {from} do {to} (Při zadávání desetinného čísla využijte desetinnou čárku, nikoliv tečku)");
                if (double.TryParse(Console.ReadLine(), out number) && number >= from && number < to)
                {
                    return true;
                }
                else if (Confirmation("Zadáno nevalidní číslo, zkusit znovu?"))
                {
                    continue;
                }
                else
                {
                    return false;
                }
            }
        }

        private static bool Confirmation(string message)
        {
            Console.WriteLine(message + " [y/n]");
            string answer = Console.ReadLine();
            return answer == "y" || answer == "Y";
        }
    }
}
