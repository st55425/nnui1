﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PrubeznaPrace2
{
    public class GeneticAlgorithm
    {
        private static Random random = new Random();
        public static readonly int DefaultGenerationRemembered = 10;
        public static readonly int MaxAverageDifference = 40;
        public static readonly int DefaultRenewLimit = 5;
        public static readonly int DefaultTournamentRounds = 2;
        public static readonly int DefaultPopulationSize = 8000;
        public static readonly double DefaultMutationRate = 0.2;

        public int Generation { get; private set; }
        public int PopulationSize { get; set; }
        public double MutationRate { get; set; }
        public Individual BestSolution { get; private set; }
        public double AverageDistance { get;private set; }
        public int TournamentRounds { get; set; }
        public int RenewLimit { get; set; }
        public int GenerationRemembered { get; set; }

        private Individual[] population;
        private int renewCount;
        private int generationsFromRenew = 0;
        private double[] averages;
        
        public GeneticAlgorithm()
        {
            GenerationRemembered = DefaultGenerationRemembered;
            RenewLimit = DefaultRenewLimit;
            TournamentRounds = DefaultTournamentRounds;
            PopulationSize = DefaultPopulationSize;
            MutationRate = DefaultMutationRate;
            
        }

        private void InitiatePopulation()
        {
            population = new Individual[PopulationSize];
            for (int i = 0; i < PopulationSize; i++)
            {
                population[i] = CreateRandomIndividual();
            }
            BestSolution = population[0];
            averages = new double[GenerationRemembered];
            Generation = 0;
            renewCount = 0;
            generationsFromRenew = 0;
        }

        private Individual CreateRandomIndividual()
        {
            List<Town> towns = new List<Town>(Town.AllTowns);
            int townsCount = towns.Count;
            Town[] randomTownsOrder = new Town[townsCount];
            for (int j = 0; j < townsCount; j++)
            {
                int position = random.Next(townsCount - j);
                randomTownsOrder[j] = towns[position];
                towns.RemoveAt(position);
            }
            return new Individual(randomTownsOrder);
        }

        public void Evolution()
        {
            InitiatePopulation();
            while (renewCount < RenewLimit) 
            {
                Generation++;
                CreateNewGeneration();               
                averages[Generation % GenerationRemembered] = AverageDistance;
                generationsFromRenew++;
                if (generationsFromRenew > GenerationRemembered && averages.Max() - MaxAverageDifference < averages.Min())
                {
                    RenewPopulation();
                    renewCount++;
                    generationsFromRenew = 0;

                }
            }
        }

        private void RenewPopulation()
        {
            for (int i = population.Length / 3; i < population.Length; i++)
            {
                population[i] = CreateRandomIndividual();
            }
        }
        private void CreateNewGeneration()
        {
            List<Individual> individuals = Selection();
            Crossover(individuals);
        }

        private List<Individual> Selection()
        {
            List<Individual> individuals = new List<Individual>();
            for (int i = 0; i < PopulationSize; i+=(int)Math.Pow(2, TournamentRounds))
            {
                Individual winner = ChooseNiceIndividual(i);
                individuals.Add(winner);
            }

            return individuals;
        }

        private Individual ChooseNiceIndividual(int startIndex)
        {
            List<Individual> participants = new List<Individual>();
            List<Individual> winners = new List<Individual>();
            for (int i = 0; i < Math.Pow(2, TournamentRounds); i++)
            {
                participants.Add(population[Math.Min(i + startIndex, PopulationSize)]);
            }
            for (int i = 0; i < TournamentRounds; i++)
            {
                winners = new List<Individual>();
                for (int j = 0; j < Math.Pow(2, TournamentRounds -1 -i); j++)
                {
                    winners.Add(CompareIndividuals(participants[2 * j], participants[2 * j + 1]));
                }
                participants = winners;
            }
            return winners[0];
        }

        private Individual CompareIndividuals(Individual first, Individual second)
        {
            if (first.Distance > second.Distance)
            {
                return second;
            }
            return first;
        }

        private void Crossover(List<Individual> parents)
        {
            double sumOfDistances = 0;
            for (int i = 0; i < PopulationSize; i++)
            {
                Individual first = parents[random.Next(parents.Count)];
                Individual second = parents[random.Next(parents.Count)];
                population[i] = first.Crossover(second);
                if (random.NextDouble() < MutationRate)
                {
                    population[i].Mutate();
                }
                UpdateBestSolution(population[i]);
                sumOfDistances += population[i].Distance;
            }
            AverageDistance = sumOfDistances / PopulationSize;
        }

        private void UpdateBestSolution(Individual individual)
        {
            if (BestSolution.Distance > individual.Distance)
            {
                BestSolution = individual;
            }
        }
    }
}
