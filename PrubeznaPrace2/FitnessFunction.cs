﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PrubeznaPrace2
{
    public static class FitnessFunction
    {
        public static double Rate(Town[] townOrder)
        {
            double totalDistance = 0;
            for (int i = 0; i < townOrder.Length; i++)
            {
                    if (townOrder[i].Distances.TryGetValue(townOrder[(i+1)%townOrder.Length], out double distance))
                    {
                        totalDistance += distance;
                    }
                } 
            return totalDistance;
        }
    }
}
