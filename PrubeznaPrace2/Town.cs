﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PrubeznaPrace2
{
    public class Town
    {

        public static List<Town> AllTowns = new List<Town>();

        public string Name { get; set; }
        public Dictionary<Town, double> Distances { get;private set; }

        public Town(string name)
        {
            Name = name;
            Distances = new Dictionary<Town, double>();
            AllTowns.Add(this);
        }

        public void InsertDistance(Town destination, double distance)
        {
            Distances.Add(destination, distance);
            destination.Distances.Add(this, distance);
        }

        public override bool Equals(object obj)
        {
            return obj is Town town &&
                   Name == town.Name;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Name);
        }

        public override string ToString()
        {
            return Name;
        }
    }
}
