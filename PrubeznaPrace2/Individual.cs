﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PrubeznaPrace2
{
    public class Individual
    {
        private static Random random = new Random();

        public double Distance { get;private set; }
        public Town[] OrderOfVisits { get;private set; }

        public int Length { get => OrderOfVisits.Length; }

        public Individual(Town[] orderOfVisits)
        {
            OrderOfVisits = orderOfVisits;
            Distance = FitnessFunction.Rate(orderOfVisits);
        }

        public void Mutate()
        {
            int first = random.Next(OrderOfVisits.Length);
            int second = random.Next(OrderOfVisits.Length);
            Town temp = OrderOfVisits[first];
            OrderOfVisits[first] = OrderOfVisits[second];
            OrderOfVisits[second] = temp;
        }

        public Individual Crossover(Individual second)
        {
            int division = random.Next(1, Length);
            Town[] crossTowns = new Town[Length];
            Array.Copy(OrderOfVisits, crossTowns, division);
            Array.Copy(second.OrderOfVisits, division, crossTowns, division, Length - division);
            return new Individual(Repair(crossTowns));
        }

        private Town[] Repair(Town[] Towns)
        {
            Town[] repairedTowns = new Town[Length]; 
            Array.Copy(Towns, repairedTowns, Length);
            HashSet<Town> allTowns = new HashSet<Town>(OrderOfVisits);
            List<int> duplicateIndex = new List<int>();

            for (int i = 0; i < Length; i++)
            {
                if (!allTowns.Remove(Towns[i]))
                {
                    duplicateIndex.Add(i);
                }
            }

            foreach (var town in allTowns)
            {
                repairedTowns[duplicateIndex[0]] = town;
                duplicateIndex.RemoveAt(0);
            }
            return repairedTowns;
        }

        public override string ToString()
        {
            string toString = "";
            foreach (var town in OrderOfVisits)
            {
                toString += town;
                toString += "->";
            }
            return toString + OrderOfVisits[0];
        }
    }
}
