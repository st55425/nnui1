﻿using Microsoft.VisualBasic.FileIO;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace PrubeznaPrace2
{
    public static class TownReader
    {

        public static void ReadTowns()
        {
            using TextFieldParser csvParser = new TextFieldParser(new StringReader(Properties.Resources.Distance));
            csvParser.SetDelimiters(new string[] { ";" });

            string[] townNames = csvParser.ReadFields();
            Dictionary<string, Town> towns = new Dictionary<string, Town>();
            for (int i = 0; i < townNames.Length; i++)
            {
                if (townNames[i].Length > 1)
                {
                    towns.Add(townNames[i],new Town(townNames[i]));
                }
            }

            while (!csvParser.EndOfData)
            {
                string[] fields = csvParser.ReadFields();
                string stringTown = fields[0];
                Town town;
                if (!towns.TryGetValue(stringTown, out town) || fields.Length > townNames.Length)
                {
                    throw new Exception("Corrupted csv file");
                }

                for (int i = 1; i < fields.Length; i++)
                {
                    if (towns.TryGetValue(townNames[i], out Town secondTown))
                    {
                        if (double.TryParse(fields[i], out double distance))
                        {
                            town.InsertDistance(secondTown, distance);
                        }
                        else if(fields[i].Length > 0)
                        {
                            throw new Exception("Corrupted csv file");
                        }
                    }                   
                }
            }
        }
    }
}
