﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PrubeznaPrace1
{


    public enum TransitionAction
    {
        None,
        TurnLeft,
        TurnRight,
        MoveForward,
        TurnBack
    }
}
