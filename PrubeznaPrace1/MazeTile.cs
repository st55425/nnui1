﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Text;

namespace PrubeznaPrace1 
{
    public class MazeTile : IComparable<MazeTile>
    {
        public int XPosition { get;private set; }

        public int YPosition { get;private set; }

        public bool Accessible { get;private set; }

        public MazeTile(int xPosition, int yPosition, bool accessible)
        {
            XPosition = xPosition;
            YPosition = yPosition;
            Accessible = accessible;
        }

        public override bool Equals(object obj)
        {
            return obj is MazeTile tile &&
                   XPosition == tile.XPosition &&
                   YPosition == tile.YPosition &&
                   Accessible == tile.Accessible;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(XPosition, YPosition, Accessible);
        }

        public override string ToString()
        {
            return $"[{XPosition},{YPosition}] - {Accessible}";
        }

        public int CompareTo([AllowNull] MazeTile other)
        {
            if (Accessible.Equals(other.Accessible))
            {
                if (XPosition.Equals(other.XPosition))
                {
                    return YPosition.CompareTo(other.YPosition);
                }
                return XPosition.CompareTo(other.XPosition);
            }
            return Accessible.CompareTo(other.Accessible);
        }
    }
}
