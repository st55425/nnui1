﻿using System;
using System.Drawing;
using System.Linq;


namespace PrubeznaPrace1
{
    class Program
    {
        private const int DefaultPossibilitesCount = 9;
        static void Main(string[] args)
        {
            Maze maze = null;
            if (args.Length > 0)
            {
                Console.WriteLine("Reading custom maze from file");
                maze = MazeReader.CreateMaze(args[0]);
            }
            if (maze == null)
            {
                Console.WriteLine("Loading default maze");
                maze = MazeReader.CreateDefaultMaze();
            }
            
            Agent agent = new Agent(maze, maze[15, 3], maze[30, 1]);

            Console.WriteLine("Vítejte v aplikaci pro hledání cesty v bludišti");
            while (true)
            {
                Console.WriteLine();
                WriteDefaultPossibilities();
                if (int.TryParse(Console.ReadLine(), out int choice) && choice > 0 && choice <= DefaultPossibilitesCount)
                {
                    switch (choice)
                    {
                        case 1:
                            agent = SetStartPoint(agent);
                            break;
                        case 2:
                            agent = SetFinalPoint(agent);
                            break;
                        case 3:
                            WriteStartPoint(agent);
                            break;
                        case 4:
                            WriteFinalPoint(agent);
                            break;
                        case 5:
                            WriteSolution(agent);
                            break;
                        case 6:
                            WriteAdditionalInfo(agent);
                            break;
                        case 7:
                            SetMaze(agent);
                            break;
                        case 8:
                            SetDefaultMaze(agent);
                            break;
                        case 9:
                            return;
                        default: 
                            Console.WriteLine("Neplatná volba");
                            break;
                    }
                }
                else
                {
                    Console.WriteLine("Neplatná volba");
                }
            }
        }
        private static void WriteDefaultPossibilities()
        {
            Console.WriteLine("Co chcete udělat?");
            Console.WriteLine("1. Zadat počáteční bod");
            Console.WriteLine("2. Zadat koncový bod");
            Console.WriteLine("3. Vypsat počáteční bod");
            Console.WriteLine("4. Vypsat koncový bod");
            Console.WriteLine("5. Vytvořit a vypsat cestu");
            Console.WriteLine("6. Vypsat dodatečné informace o cestě");
            Console.WriteLine("7. Změnit bludiště");
            Console.WriteLine("8. Změnit bludiště na základní");
            Console.WriteLine("9. Ukončit aplikaci");

        }

        private static Agent SetStartPoint(Agent agent)
        {
            MazeTile enteredPoint = EnterPoint(agent.ExaminedMaze);
            if (enteredPoint != null)
            {
                agent.StartPosition = enteredPoint;
                agent.FindPath();
            }
            else
            {
                Console.WriteLine("Počáteční bod nebyl změněn");
            }
            WriteStartPoint(agent);
            return agent;
        }

        private static Agent SetFinalPoint(Agent agent)
        {
            MazeTile enteredPoint = EnterPoint(agent.ExaminedMaze);
            if (enteredPoint != null)
            {
                agent.FinalPosition = enteredPoint;
                agent.FindPath();
            }
            else
            {
                Console.WriteLine("Koncový bod nebyl změněn");
            }
            WriteFinalPoint(agent);
            return agent;
        }

        private static void WriteStartPoint(Agent agent)
        {
            Console.WriteLine("Počáteční bod je:");
            Console.WriteLine(agent.StartPosition.ToString());
        }

        private static void WriteFinalPoint(Agent agent)
        {
            Console.WriteLine("Koncový bod je:");
            Console.WriteLine(agent.FinalPosition.ToString());
        }

        private static void WriteSolution(Agent agent)
        {
            if (agent.SolutionExist)
            {
                foreach (var item in agent.SolutionPath)
                {
                    Console.WriteLine(item);
                }
            }
            else
            {
                Console.WriteLine("Cesta z počátečního bodu do koncového neexistuje");
            }
        }

        private static void WriteAdditionalInfo(Agent agent)
        {
            if (agent.SolutionExist)
            {
                Console.WriteLine($"Number of expanded nodes: {agent.ExploredCount}");
                Console.WriteLine($"Number of moves: {agent.SolutionPath.Count}");
                Console.WriteLine($"Initial expected cost: {agent.SolutionPath.First().EstimatedTotalCost}");
                Console.WriteLine($"Shortest path cost: {agent.SolutionPath.Last().PathCost}");
            }
            else
            {
                Console.WriteLine("Cesta z počátečního bodu do koncového neexistuje");
            }
        }
        private static Agent SetMaze(Agent agent)
        {
            Console.WriteLine("Zadejte cestu k souboru s bludištěm:");
            string path = Console.ReadLine();
            Maze maze = MazeReader.CreateMaze(path);
            while (maze == null)
            {
                if (Confirmation("Chcete zadat novou cestu k souboru?"))
                {
                    path = Console.ReadLine();
                    maze = MazeReader.CreateMaze(path);
                }
                else
                {
                    Console.WriteLine("Bludiště nebylo změněno");
                    break;
                }
            }
            if (maze != null)
            {
                agent.StartPosition = maze[0, 0];
                agent.FinalPosition = maze[0, 0];
                agent.FindPath();
            }
            return agent;
        }

        private static Agent SetDefaultMaze(Agent agent)
        {
            agent.ExaminedMaze = MazeReader.CreateDefaultMaze();
            agent.StartPosition = agent.ExaminedMaze[1, 1];
            agent.FinalPosition = agent.ExaminedMaze[3, 3];
            Console.WriteLine("Načteno základní bludiště");
            WriteStartPoint(agent);
            WriteFinalPoint(agent);
            agent.FindPath();
            return agent;
        }


        private static MazeTile EnterPoint(Maze maze)
        {
            int xPosition;
            int yPosition;
            while (true)
            {
                Console.WriteLine($"Zadejte souřadnici x v rozsahu 0-{maze.Width - 1}");
                if (int.TryParse(Console.ReadLine(), out xPosition) && xPosition >=0 && xPosition < maze.Width)
                {
                    Console.WriteLine($"Zadejte souřadnici y v rozsahu 0-{maze.Height - 1}");
                    if (int.TryParse(Console.ReadLine(), out yPosition) && yPosition >= 0 && yPosition < maze.Height)
                    {
                        if (maze[xPosition, yPosition].Accessible)
                            return maze[xPosition, yPosition];
                        else if (Confirmation("Zadaný bod není přístupný, zkusit znovu?"))
                            continue;
                        else
                            return null;

                    }else
                    {
                        if (Confirmation("Zadal jste neplatnou souřadnici y, zkusit znovu?"))
                            continue;
                        else
                            return null;
                    }
                }
                else
                {
                    if (Confirmation("Zadal jste neplatnou souřadnici x, zkusit znovu?"))
                        continue;
                    else
                        return null;
                }
            }
        }

        private static bool Confirmation(string message)
        {
            Console.WriteLine(message + " [y/n]");
            string answer = Console.ReadLine();
            return answer == "y" || answer == "Y";
        }
    }
}
