﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Text;

namespace PrubeznaPrace1
{
    public class Agent
    {
        public static readonly ImmutableDictionary<TransitionAction, int> TransitionCost = new Dictionary<TransitionAction, int>()
        {
            {TransitionAction.TurnLeft, 1 },
            {TransitionAction.TurnRight, 1 },
            {TransitionAction.TurnBack, 2 },
            {TransitionAction.MoveForward, 3 }
        }.ToImmutableDictionary();

        public MazeTile StartPosition { get; set; }

        public MazeTile FinalPosition { get; set; }

        public List<GraphNode> SolutionPath { get;private set; }

        public bool SolutionExist { get => SolutionPath.Count>0; }

        public Maze ExaminedMaze { get; set; }

        public int ExploredCount { get;private set; }

        private HashSet<MazeTile> explored;
        private SortedSet<GraphNode> fringe;
        private HeuristicFunction heuristic;

        public Agent(Maze examinedMaze, MazeTile startPosition, MazeTile finalPosition)
        {
            StartPosition = startPosition;
            FinalPosition = finalPosition;
            ExaminedMaze = examinedMaze;
            SolutionPath = new List<GraphNode>();
            explored = new HashSet<MazeTile>();
            fringe = new SortedSet<GraphNode>();
            heuristic = new HeuristicFunction();
            FindPath();
        }
        private void ResetValues()
        {
            SolutionPath.Clear();
            explored.Clear();
            fringe.Clear();
            ExploredCount = 0;
        }

        public void FindPath()
        {
            ResetValues();
            if (!StartPosition.Accessible || !FinalPosition.Accessible)
            {
                return;
            }
            fringe.Add(heuristic.ComputeCost(new GraphNode(StartPosition, 0, 0, TransitionAction.None, null, WorldSide.North), FinalPosition));
            while (fringe.Count >0)
            {
                GraphNode expanded = fringe.Min;
                fringe.Remove(fringe.Min);
                if (expanded.Tile.Equals(FinalPosition))
                {
                    ReconstructPath(expanded);
                    return;
                }
                FindAllSuccessors(expanded);
                explored.Add(expanded.Tile);
                ExploredCount++;
            }
        }

        private void ReconstructPath(GraphNode finalNode)
        {
            GraphNode node = finalNode;
            List<GraphNode> path = new List<GraphNode>();
            while (node != null)
            {
                path.Add(node);
                node = node.Parent;
            }
            path.Reverse();
            SolutionPath = path;
        }

        private void FindAllSuccessors(GraphNode examined)
        {
            foreach (var side in Enum.GetValues(typeof(WorldSide)))
            {
                MazeTile possibleSuccessor = ExaminedMaze.GetNeighbourgh(examined.Tile, (WorldSide)side);
                if (possibleSuccessor != null && possibleSuccessor.Accessible)
                {
                    AddToFringe(CreateSuccessor((WorldSide)side, examined));
                }                
            }
        }

        private void AddToFringe(GraphNode node)
        {
            if (explored.Contains(node.Tile))
            {
                return;
            }
            if (fringe.Contains(node))
            {
                fringe.TryGetValue(node, out GraphNode actual);
                if (node.EstimatedTotalCost < actual.EstimatedTotalCost)
                {
                    fringe.Remove(actual);
                }
            }
            fringe.Add(node);
        }

        private GraphNode CreateSuccessor(WorldSide examinedOrientation, GraphNode parent)
        {
            int azimut = examinedOrientation - parent.Orientation;
            if (azimut < 0)
            {
                azimut += 360;
            }

            MazeTile newPosition = parent.Tile;
            TransitionAction action;
            switch (azimut)
            {
                case 0:
                    action = TransitionAction.MoveForward;
                    newPosition = ExaminedMaze.GetNeighbourgh(parent.Tile, examinedOrientation);
                    break;
                case 90: 
                    action = TransitionAction.TurnRight;
                    break;
                case 180: 
                    action = TransitionAction.TurnBack;
                    break;
                case 270: 
                    action = TransitionAction.TurnLeft;
                    break;
                default:
                    throw new Exception("Unexpected azimut");
            }
            int pathCost = parent.PathCost + TransitionCost[action];
            return heuristic.ComputeCost(new GraphNode(newPosition, pathCost, 0, action, parent, examinedOrientation), FinalPosition);
        }

    }
}
