﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Text;

namespace PrubeznaPrace1
{
    public class Maze
    {

        private MazeTile[,] maze;

        public int Width { get => maze.GetLength(1);}

        public int Height { get => maze.GetLength(0);}

        public Maze(MazeTile[,] maze)
        {
            this.maze = maze;
        }

        public MazeTile this[int x, int y] => maze[y,x];

        public MazeTile GetNeighbourgh(MazeTile position, WorldSide side)
        {
            return side switch
            {
                WorldSide.North => GetNorthernTile(position),
                WorldSide.East => GetEasternTile(position),
                WorldSide.South => GetSouthernTile(position),
                WorldSide.West => GetWesternTile(position),
                _ => throw new Exception("Unexpected world side"),
            };
        }
        private MazeTile GetNorthernTile(MazeTile position)
        {
            if (position.YPosition > 0)
            {
                return this[position.XPosition, position.YPosition - 1];
            }
            return null;
        }

        private MazeTile GetEasternTile(MazeTile position)
        {
            if (position.XPosition < Height)
            {
                return this[position.XPosition + 1, position.YPosition];
            }
            return null;
        }

        private MazeTile GetSouthernTile(MazeTile position)
        {
            if (position.YPosition < Height)
            {
                return this[position.XPosition, position.YPosition + 1];
            }
            return null;
        }

        private MazeTile GetWesternTile(MazeTile position)
        {
            if (position.XPosition > 0)
            {
                return this[position.XPosition - 1, position.YPosition];
            }
            return null;
        }





    }
}
