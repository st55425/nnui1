﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Text;

namespace PrubeznaPrace1
{
    public class GraphNode : IComparable<GraphNode>
    {
        public MazeTile Tile { get;private set; }

        public int PathCost { get;private set; }

        public int EstimatedTotalCost { get; set; }

        public TransitionAction Action { get;private set; }

        public GraphNode Parent { get;private set; }

        public WorldSide Orientation { get;private set; }

        public GraphNode(MazeTile tile, int pathCost, int estimatedTotalCost, TransitionAction action, GraphNode parent, WorldSide orientation)
        {
            Tile = tile;
            PathCost = pathCost;
            EstimatedTotalCost = estimatedTotalCost;
            Action = action;
            Parent = parent;
            Orientation = orientation;
        }

        public override string ToString()
        {
            return $"{Action} \n[{Tile.XPosition},{Tile.YPosition}] ";
        }

        public override bool Equals(object obj)
        {
            return obj is GraphNode node &&
                   EqualityComparer<MazeTile>.Default.Equals(Tile, node.Tile) &&
                   Action == node.Action &&
                   Orientation == node.Orientation;
        }

        public int CompareTo([AllowNull] GraphNode other)
        {
            if (EstimatedTotalCost.Equals(other.EstimatedTotalCost))
            {
                if (PathCost.Equals(other.PathCost))
                {
                    if (Tile.Equals(other.Tile))
                    {
                         return Orientation.CompareTo(other.Orientation);
                    }
                    return Tile.CompareTo(other.Tile);
                }
                return PathCost.CompareTo(other.PathCost);
            }
            return EstimatedTotalCost.CompareTo(other.EstimatedTotalCost);
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Tile, PathCost, EstimatedTotalCost, Action, Parent, Orientation);
        }
    }
}
