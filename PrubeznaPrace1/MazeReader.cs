﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Text;
using System.Resources;
using System.Reflection;


namespace PrubeznaPrace1
{
    public static class MazeReader
    {
        public static Maze CreateMaze(string filePath)
        {
            Image mazeImage = ReadImage(filePath);
            if (mazeImage == null)
            {
                return null;
            }
            return MazeFromImage(new Bitmap(mazeImage));
        }

        public static Maze CreateDefaultMaze()
        {
            return MazeFromImage(Properties.Resources.MAZE1);
        }

        private static Image ReadImage(string filePath)
        {
            Image mazeImage = null;
            try
            {
                mazeImage = Image.FromFile(filePath);
            }catch(FileNotFoundException)
            {
                Console.WriteLine("Soubor s bludištěm nebyl nalezen");
            }
            catch
            {
                Console.WriteLine("Soubor se nepodařilo načíst");
            }
            return mazeImage;
        }

        private static Maze MazeFromImage(Bitmap mazeImage)
        {
            MazeTile[,] mazeTiles = new MazeTile[mazeImage.Height, mazeImage.Width];
            for (int i = 0; i < mazeImage.Height; i++)
            {
                for (int j = 0; j < mazeImage.Width; j++)
                {
                    if (mazeImage.GetPixel(j,i).ToArgb()==Color.White.ToArgb())
                    {
                        mazeTiles[i, j] = new MazeTile(j, i, true);
                    }
                    else
                    {
                        mazeTiles[i, j] = new MazeTile(j, i, false);
                    }
                }
            }

            return new Maze(mazeTiles);
        }
    }
}
