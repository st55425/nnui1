﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PrubeznaPrace1
{
    public class HeuristicFunction
    {
        private const int DistanceCost = 5;
        private const int TurnBackPenalty = 10000;
        private const int FirstBackStep = 2;

        public GraphNode ComputeCost(GraphNode examinedPosition, MazeTile finalPosition)
        {
            int distance = Math.Abs(examinedPosition.Tile.XPosition - finalPosition.XPosition) + 
                Math.Abs(examinedPosition.Tile.YPosition - finalPosition.YPosition);

            examinedPosition.EstimatedTotalCost = examinedPosition.PathCost + distance * DistanceCost;
            if (examinedPosition.Action.Equals(TransitionAction.TurnBack) && examinedPosition.PathCost > FirstBackStep)
            {
                examinedPosition.EstimatedTotalCost += TurnBackPenalty;
            }
            return examinedPosition;
        }

    }
}
