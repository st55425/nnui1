﻿
using Accord.Fuzzy;
using System;

namespace Cviceni6
{
    class Program
    {
        static void Main(string[] args)
        {
            var supply = new LinguisticVariable("ZásobaBrambor", 100, 1000);
            var smallSupply = new FuzzySet("Malá", new TrapezoidalFunction(100, 100, 450)); 
            var mediumSupply = new FuzzySet("Střední", new TrapezoidalFunction(350, 550, 750));
            var bigSupply = new FuzzySet("Velká", new TrapezoidalFunction(650, 1000, 1000));
            supply.AddLabel(smallSupply);
            supply.AddLabel(mediumSupply);
            supply.AddLabel(bigSupply);

            var soldLast7Days = new LinguisticVariable("ProdanéMnožstvíZaPosledních7Dní", 200, 2000);
            var smallSolds = new FuzzySet("Malá", new TrapezoidalFunction(200, 200, 900)); 
            var mediumSolds = new FuzzySet("Střední", new TrapezoidalFunction(700, 1100, 1500)); 
            var bigSolds = new FuzzySet("Velká", new TrapezoidalFunction(1300, 2000, 2000));
            soldLast7Days.AddLabel(smallSolds);
            soldLast7Days.AddLabel(mediumSolds);
            soldLast7Days.AddLabel(bigSolds);

            var pricePerKg = new LinguisticVariable("CenaBramborZa1Kg", 3, 25);
            var dumpingPrice = new FuzzySet("Dumpingová", new TrapezoidalFunction(3, 3, 12));
            var lowPrice = new FuzzySet("Nízká", new TrapezoidalFunction(7, (float)11.5, 16));
            var highPrice = new FuzzySet("Vysoká", new TrapezoidalFunction(11, 16, 21)); 
            var overpricedPrice = new FuzzySet("Předražená", new TrapezoidalFunction(16, 25, 25));
            pricePerKg.AddLabel(dumpingPrice);
            pricePerKg.AddLabel(lowPrice);
            pricePerKg.AddLabel(highPrice);
            pricePerKg.AddLabel(overpricedPrice);

            Database fuzzyDatabase = new Database();
            fuzzyDatabase.AddVariable(supply);
            fuzzyDatabase.AddVariable(soldLast7Days);
            fuzzyDatabase.AddVariable(pricePerKg);

            InferenceSystem inferenceSystem = new InferenceSystem(fuzzyDatabase, new CentroidDefuzzifier(1000));
            inferenceSystem.NewRule("rule1", "IF ZásobaBrambor IS Velká AND ProdanéMnožstvíZaPosledních7Dní IS Malá THEN CenaBramborZa1Kg IS Dumpingová");
            inferenceSystem.NewRule("rule2", "IF ZásobaBrambor IS Střední AND ProdanéMnožstvíZaPosledních7Dní IS Malá THEN CenaBramborZa1Kg IS Nízká");
            inferenceSystem.NewRule("rule3", "IF ZásobaBrambor IS Malá AND ProdanéMnožstvíZaPosledních7Dní IS Malá THEN CenaBramborZa1Kg IS Vysoká");
            inferenceSystem.NewRule("rule4", "IF ZásobaBrambor IS Velká AND ProdanéMnožstvíZaPosledních7Dní IS Střední THEN CenaBramborZa1Kg IS Nízká");
            inferenceSystem.NewRule("rule5", "IF ZásobaBrambor IS Střední AND ProdanéMnožstvíZaPosledních7Dní IS Střední THEN CenaBramborZa1Kg IS Nízká");
            inferenceSystem.NewRule("rule6", "IF ZásobaBrambor IS Malá AND ProdanéMnožstvíZaPosledních7Dní IS Střední THEN CenaBramborZa1Kg IS Vysoká");
            inferenceSystem.NewRule("rule7", "IF ZásobaBrambor IS Velká AND ProdanéMnožstvíZaPosledních7Dní IS Velká THEN CenaBramborZa1Kg IS Vysoká");
            inferenceSystem.NewRule("rule8", "IF ZásobaBrambor IS Střední AND ProdanéMnožstvíZaPosledních7Dní IS Velká THEN CenaBramborZa1Kg IS Vysoká");
            inferenceSystem.NewRule("rule9", "IF ZásobaBrambor IS Malá AND ProdanéMnožstvíZaPosledních7Dní IS Velká THEN CenaBramborZa1Kg IS Předražená");

            inferenceSystem.SetInput("ZásobaBrambor", 200);
            inferenceSystem.SetInput("ProdanéMnožstvíZaPosledních7Dní", 500);
            var result1 = inferenceSystem.Evaluate("CenaBramborZa1Kg");
            Console.WriteLine($"Cena brambor pro skladové zásoby 200kg a prodané množství v předchozích 7 dnech 500 kg je: {result1}");

            inferenceSystem.SetInput("ZásobaBrambor", 300);
            inferenceSystem.SetInput("ProdanéMnožstvíZaPosledních7Dní", 1800);
            var result2 = inferenceSystem.Evaluate("CenaBramborZa1Kg");
            Console.WriteLine($"Cena brambor pro skladové zásoby 300kg a prodané množství v předchozích 7 dnech 1800 kg je: {result2}");
        }
    }
}
