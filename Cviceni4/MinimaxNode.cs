﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Cviceni4
{
    public class MinimaxNode
    {
        public Board GameBoard { get; set; }

        public int? Score { get; set; }

        public int SignedCell { get; set; }

        public MinimaxNode(Board gameBoard, int signedCell)
        {
            GameBoard = gameBoard;
            SignedCell = signedCell;
            Score = null;
        }

        public override string ToString()
        {
            return "Signed cell: " + SignedCell + ", Score: " + Score;
        }
    }
}