﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Cviceni4
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private Game game;
        private Minimax AIPlayer;
        private Minimax AIPlayer2;
        public MainWindow()
        {
            InitializeComponent();
            game = new Game();
            DataContext = game;
            BoardContainer.DataContext = game;
            AIPlayer = new Minimax(game, "X");
            AIPlayer2 = new Minimax(game, "O");
        }

        private void CellClick(object sender, RoutedEventArgs e)
        {
            var cell = (sender as Button).DataContext as Cell;
            game.Move(cell);
            if (!CheckEndGame())
            {
                AIPlay();
            }
        }

        private void AIPlay()
        {
            while (true)
            {
                if ((bool)XPlayerAI.IsChecked && game.FirstPlayer)
                {
                    AIPlayer.NextMove();
                    if (CheckEndGame())
                    {
                        return;
                    }                   
                }
                else if ((bool)YPlayerAI.IsChecked && !game.FirstPlayer)
                {
                    AIPlayer2.NextMove();
                    if (CheckEndGame())
                    {
                        return;
                    }
                }
                else
                {
                    return;
                }
            }
            

        }

        private bool CheckEndGame()
        {
            if (game.EndGame)
            {
                MessageBoxResult newGamePopup = MessageBox.Show("Konec hry, začít novou?", "Confirm", MessageBoxButton.YesNo);
                if (newGamePopup == MessageBoxResult.Yes)
                {
                    game.NewGame();
                }
                else
                {
                    Environment.Exit(0);
                }
                return true;
            }
            return false;
        }
        private void StartButtonClick(object sender, RoutedEventArgs e)
        {
            game.StartGame();
            AIPlay();
        }

        private void ClearButtonClick(object sender, RoutedEventArgs e)
        {
            game.NewGame();
        }

        private void Default1ButtonClick(object sender, RoutedEventArgs e)
        {
            game.NewGame();
            game.Move(game.GameBoard[0,0]);
            game.Move(game.GameBoard[0,1]);
            game.Move(game.GameBoard[1,2]);
            game.Move(game.GameBoard[1,1]);
        }

        private void Default2ButtonClick(object sender, RoutedEventArgs e)
        {
            game.NewGame();
            game.Move(game.GameBoard[0, 1]);
            game.Move(game.GameBoard[0, 0]);
            game.Move(game.GameBoard[1, 0]);
            game.Move(game.GameBoard[1, 1]);
            game.Move(game.GameBoard[2, 1]);
            game.Move(game.GameBoard[1, 2]);
        }

        private void Default3ButtonClick(object sender, RoutedEventArgs e)
        {
            game.NewGame();
            game.Move(game.GameBoard[0, 0]);
            game.Move(game.GameBoard[0, 1]);
            game.Move(game.GameBoard[1, 1]);
            game.Move(game.GameBoard[2, 2]);
        }
        private void XPlayerCheckboxClick(object sender, RoutedEventArgs e)
        { 

        }

        private void YPlayerCheckboxClick(object sender, RoutedEventArgs e)
        {
        }
    }
}
