﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cviceni4
{
    public class Utility
    {
        private const int CellsToWin = 3;

        public static bool EndGame(Board board, out int score)
        {
            score = Result(board);
            return FullBoard(board) || score != 0;
        }
        private static int Result(Board board)
        {
            if (ResultInDirection(0, board.Rows, board.GetRow, out int score) ||
                ResultInDirection(0, board.Columns, board.GetColumn, out score) ||
                ResultInDirection(-board.Rows + 1, board.Columns - 1, board.GetMainDiagonal, out score) ||
                ResultInDirection(-board.Columns + 1, board.Rows - 1, board.GetSecondaryDiagonal, out score))
            {
                return score;
            }
            return 0;
        }

        private static bool FullBoard(Board board)
        {
            if (board.Cells.All(x => x.Sign != ""))
            {
                return true;
            }
            return false;
        }

        private static bool ResultInDirection(int min, int max, Func<int, Cell[]> RowProducent, out int score)
        {
            for (int i = min; i < max; i++)
            {
                string winner = CheckCellRow(RowProducent(i));
                if (winner.Length > 0)
                {
                    if (winner == "X")
                    {
                        score = 1;
                    }
                    else
                    {
                        score = -1;
                    }
                    return true;
                }
            }
            score = 0;
            return false;
        }

        private static string CheckCellRow(Cell[] cellRow)
        {
            if (cellRow.Length < CellsToWin)
            {
                return "";
            }
            int sameInRow = 0;
            string lastSign = "";
            for (int i = 0; i < cellRow.Length; i++)
            {
                if (cellRow[i].CanSelect)
                {
                    lastSign = "";
                    sameInRow = 0;
                }
                else if (cellRow[i].Sign == lastSign)
                {
                    sameInRow++;
                    if (sameInRow == CellsToWin)
                    {
                        return lastSign;
                    }
                }
                else
                {
                    sameInRow = 1;
                    lastSign = cellRow[i].Sign;
                }
            }
            return "";
        }
    }
}
