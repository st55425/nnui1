﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace Cviceni4
{
    public class Cell : INotifyPropertyChanged
    {

        private string sign;
        private bool canSelect;

        public string Sign
        {
            get { return sign; }
            set { sign = value;
                if (value == "")
                {
                    CanSelect = true;
                }
                else
                {
                    CanSelect = false;
                }
                RaisePropertyChanged("Sign");
            }
        }

        public bool CanSelect
        {
            get { return canSelect; }
            set { canSelect = value;
                RaisePropertyChanged("CanSelect");
            }
        }

        public Cell()
        {
            CanSelect = true;
        }

        public Cell(Cell cell)
        {
            Sign = cell.Sign;
            CanSelect = cell.CanSelect;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void RaisePropertyChanged(string property)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(property));
            }
        }

        public override string ToString()
        {
            return sign;
        }
    }
}
