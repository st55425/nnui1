﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Windows.Media;

namespace Cviceni4
{
    public class Minimax
    {
        private const string MaxPlayer = "X";
        private const string MinPlayer = "O";
        private int debugDepth = 0;

        public string Player { get; private set; }

        public Game Game { get; set; }

        public Minimax(Game game, string player)
        {
            if (player != MaxPlayer && player != MinPlayer)
            {
                throw new ArgumentException("Undefined player");
            }
            Player = player;
            Game = game;
        }

        public void NextMove()
        {
            MinimaxNode nextMove;
            if (Player == MaxPlayer)
            {
                nextMove = EvaluateNodeMax(new MinimaxNode(Game.GameBoard, 0));
            }
            else
            {
                nextMove = EvaluateNodeMin(new MinimaxNode(Game.GameBoard, 0));
            }
            Game.Move(Game.GameBoard.Cells[nextMove.SignedCell]);
            /*if (Player == MaxPlayer)
            {
                List<MinimaxNode> sucessors = GetAllPossibleMoves(new MinimaxNode(Game.GameBoard, -1), MaxPlayer);
                foreach (var item in sucessors)
                {
                    EvaluateNodeMax(item, "X");
                }
                MinimaxNode nextMove = sucessors.Find(x => x.Score == 1);
                if (nextMove == null)
                {
                    nextMove = sucessors.Find(x => x.Score == 0);
                }
                if (nextMove == null)
                {
                    nextMove = sucessors[0];
                }
                Game.Move(Game.GameBoard.Cells[nextMove.SignedCell]); 
            }*/
        }

        private void EvaluateNodeMax(MinimaxNode evaluatedState, string player)
        {
            int maxScore = int.MinValue;
            List<MinimaxNode> successors = GetAllPossibleMoves(evaluatedState, player);
            for (int i = 0; i < successors.Count; i++)
            {
                 if (Utility.EndGame(successors[i].GameBoard, out int score))
                    {
                        if (score > maxScore)
                        {
                            maxScore = score;
                        }
                    }
                    else
                    {
                        EvaluateNodeMin(successors[i], "X");
                        if (successors[i].Score > maxScore)
                        {
                            maxScore = score;
                        }
                    }
                
            }
            evaluatedState.Score = maxScore;
        }

        private void EvaluateNodeMin(MinimaxNode evaluatedState, string player)
        {
            int minScore = int.MaxValue;
            List<MinimaxNode> successors = GetAllPossibleMoves(evaluatedState, player);
            for (int i = 0; i < successors.Count; i++)
            {
                if (Utility.EndGame(successors[i].GameBoard, out int score))
                {
                    if (score < minScore)
                    {
                        minScore = score;
                    }
                }
                else
                {
                    EvaluateNodeMax(successors[i], "O");
                    if (successors[i].Score < minScore)
                    {
                        minScore = score;
                    }
                }

            }
            evaluatedState.Score = minScore;
        }

        private MinimaxNode EvaluateNodeMax(MinimaxNode evaluatedState)
        {
            int maxScore = int.MinValue;
            MinimaxNode bestMove = null;
            for (int i = 0; i < Game.GameBoard.Cells.Count; i++)
            {
                if (TryGetChildNode(evaluatedState, i, MaxPlayer, out MinimaxNode move))
                {
                    if (Utility.EndGame(move.GameBoard, out int score))
                    {
                        move.Score = score;
                        if (score > maxScore)
                        {
                            maxScore = score;
                            bestMove = move;
                        }
                    }
                    else
                    {
                        EvaluateNodeMin(move);
                        if (move.Score > maxScore)
                        {
                            maxScore = (int)move.Score;
                            bestMove = move;
                        }
                    }
                }             
            }
            evaluatedState.Score = maxScore;
            return bestMove;
        }

        private MinimaxNode EvaluateNodeMin(MinimaxNode evaluatedState)
        {
            int minScore = int.MaxValue;
            MinimaxNode bestMove = null;
            for (int i = 0; i < Game.GameBoard.Cells.Count; i++)
            {
                if (TryGetChildNode(evaluatedState, i, MinPlayer, out MinimaxNode move))
                {
                    if (Utility.EndGame(move.GameBoard, out int score))
                    {
                        move.Score = score;
                        if (score < minScore)
                        {
                            minScore = score;
                            bestMove = move;
                        }
                    }
                    else
                    {
                        EvaluateNodeMax(move);
                        if (move.Score < minScore)
                        {
                            minScore = (int)move.Score;
                            bestMove = move;
                        }
                    }
                }
            }
            evaluatedState.Score = minScore;
            debugDepth--;
            return bestMove;
        }
        private List<MinimaxNode> GetAllPossibleMoves(MinimaxNode examined, string player)
        {
            List<MinimaxNode> nodes = new List<MinimaxNode>();
            for (int i = 0; i < examined.GameBoard.Cells.Count; i++)
            {
                if (examined.GameBoard.Cells[i].CanSelect)
                {
                    Board nextMove = new Board(examined.GameBoard);
                    nextMove.Cells[i].Sign = player;
                    nodes.Add(new MinimaxNode(nextMove, i));
                }
            }
            return nodes;
        }

        private bool TryGetChildNode(MinimaxNode node, int action, string player, out MinimaxNode child)
        {
            if (node.GameBoard.Cells[action].CanSelect)
            {
                Board nextMove = new Board(node.GameBoard);
                nextMove.Cells[action].Sign = player;
                child = new MinimaxNode(nextMove, action);
                return true;
            }
            else
            {
                child = null;
                return false;
            }
        }
    }
}
