﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace Cviceni4
{
    public class Board
    {

        public int Rows { get;private set; }
        public int Columns { get;private  set; }

        public Cell this[int row, int column] => Cells[row*Rows+column];

        public ObservableCollection<Cell> Cells { get; private set;}

        public Board(int rows, int columns)
        {
            Rows = rows;
            Columns = columns;
            Cells = new ObservableCollection<Cell>(Enumerable.Range(0, Rows * Columns).Select(i => new Cell()));
        }

        public Board(Board board)
        {
            Rows = board.Rows;
            Columns = board.Columns;
            Cells = new ObservableCollection<Cell>();
            foreach (var cell in board.Cells)
            {
                Cells.Add(new Cell(cell));
            }
        }

        public void NewGame()
        {
            foreach (var cell in Cells)
            {
                cell.Sign = "";
            }
        }

        public Cell[] GetRow(int row)
        {
            if (row < 0 && row > Rows)
            {
                throw new ArgumentException("Řádek hracího pole neexistuje");
            }
            Cell[] cellRow = new Cell[Columns];
            for (int i = 0; i < Columns; i++)
            {
                cellRow[i] = this[row, i];
            }
            return cellRow;
        }

        public Cell[] GetColumn(int column)
        {
            if (column < 0 && column > Columns)
            {
                throw new ArgumentException("Sloupec hracího pole neexistuje");
            }
            Cell[] cellColumn = new Cell[Rows];
            for (int i = 0; i < Rows; i++)
            {
                cellColumn[i] = this[i, column];
            }
            return cellColumn;
        }

        public Cell[] GetMainDiagonal(int offset)
        {
            if (offset <= -Rows && offset >= Columns)
            {
                throw new ArgumentException("Diagonála pole neexistuje");
            }
            List<Cell> cells = new List<Cell>();
            for (int i = 0; i < Rows; i++)
            {
                if (i+offset >= 0 && i+offset < Columns)
                {
                    cells.Add(this[i, i + offset]);
                }
            }
            return cells.ToArray();
        }

        public Cell[] GetSecondaryDiagonal(int offset)
        {
            if (offset <= -Rows && offset >= Columns)
            {
                throw new ArgumentException("Diagonála pole neexistuje");
            }
            List<Cell> cells = new List<Cell>();
            for (int i = 0; i < Rows; i++)
            {
                if (i + offset >= 0 && Columns - i + offset-1 < Columns)
                {
                    cells.Add(this[i, Columns-i+offset-1]);
                }
            }
            return cells.ToArray();
        }

        public override string ToString()
        {
            string s = "";
            for (int i = 0; i < Cells.Count; i++)
            {
                s += Cells[i].ToString();
                if (i != Cells.Count-1)
                {
                    s += ",";
                }
            }
            return s;
        }
    }
}
