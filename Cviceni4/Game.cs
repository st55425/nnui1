﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Windows.Input;

namespace Cviceni4
{
    public class Game : INotifyPropertyChanged
    {
        private const int CellsToWin = 3;

        private int xScore;
        private int oScore;

        public event PropertyChangedEventHandler PropertyChanged;
        public Board GameBoard { get;private set; }

        public bool FirstPlayer { get;private set; }

        public bool EndGame { get;private set; }

        public int XScore 
        { 
            get => xScore;
            private set
            {
                xScore = value;
                RaisePropertyChanged("XScore");
            }
        }
        
        public int OScore
        {
            get => oScore;
            private set
            {
                oScore = value;
                RaisePropertyChanged("OScore");
            }
        }

        public Game()
        {
            GameBoard = new Board(3, 3);
            NewGame();
        }

        public void StartGame()
        {
            foreach (var item in GameBoard.Cells)
            {
                if (item.Sign == "")
                {
                    item.CanSelect = true;
                }              
            }
        }

        public void NewGame()
        {
            GameBoard.NewGame();
            FirstPlayer = true;
            EndGame = false;
            foreach (var item in GameBoard.Cells)
            {
                item.CanSelect = false;
            }
        }

        public void Move(Cell cell)
        {
            cell.Sign = FirstPlayer ? "X" : "O";
            FirstPlayer = !FirstPlayer;
            if (Utility.EndGame(GameBoard, out int score))
            {
                EndGame = true;
                ChangeScore(score);
            }
        }


        public void ChangeScore(int score)
        {
            XScore += score;
            OScore -= score;
        }

        private void RaisePropertyChanged(string property)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(property));
            }
        }
    }
}
